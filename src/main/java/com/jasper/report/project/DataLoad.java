package com.jasper.report.project;

import com.jasper.report.project.entity.Employee;
import com.jasper.report.project.repository.EmployeeRepository;
import jakarta.annotation.PostConstruct;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataLoad {

  @Autowired
  private EmployeeRepository repository;

  @PostConstruct
  void loadData() {
    Employee employee1 = new Employee(1, "Kirill", 20, "Saint Petersubrg", 300.);
    Employee employee2 = new Employee(2, "Vseslav", 1, "Saint Petersburg", 150.);
    Employee employee3 = new Employee(3, "Kirill", 20, "Saint Petersburg", 200.);

    repository.saveAll(List.of(employee1, employee2, employee3));
  }
}
