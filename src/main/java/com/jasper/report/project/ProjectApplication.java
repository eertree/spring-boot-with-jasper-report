package com.jasper.report.project;

import com.jasper.report.project.entity.Employee;
import com.jasper.report.project.repository.EmployeeRepository;
import com.jasper.report.project.service.ReportService;
import java.io.FileNotFoundException;
import java.util.List;
import net.sf.jasperreports.engine.JRException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ProjectApplication {

	private final EmployeeRepository employeeRepository;
	private final ReportService reportService;

	public ProjectApplication(EmployeeRepository employeeRepository, ReportService reportService) {
		this.employeeRepository = employeeRepository;
		this.reportService = reportService;
	}

	@GetMapping("/employees")
	List<Employee> getEmployees() {
		return employeeRepository.findAll();
	}

	@GetMapping("/report/{format}")
	public String generateReport(@PathVariable String format)
			throws JRException, FileNotFoundException {
		return reportService.exportReport(format);
	}

	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}

}
